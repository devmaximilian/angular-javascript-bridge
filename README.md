# Angular Javascript Bridge

Javascript class to retrieve data from Angular 7

## Drivers

A driver referes to a form of local storage that can be used to communicate data. Only use cookie driver if your data is < 4 KB. Available drivers include `localstorage` (`l`), `indexeddb` (`i`), `cookie` (`c`), `sessionstorage` (`s`) and `auto` (`a`). The auto option will determine what driver to use depending on client compatability. **Note that your Angular project needs write to the same storage as the driver**.
