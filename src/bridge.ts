class Bridge {
    // Storage drivers
    private drivers: {
        available: {
            localStorage: boolean;
            indexedDB: boolean;
            cookie: boolean;
            sessionStorage: boolean;
            auto: boolean;
        };
    } = {
        available: {
            localStorage: false,
            indexedDB: false,
            cookie: false,
            sessionStorage: false,
            auto: false
        }
    };
    // Selected driver
    private driver: string = "auto";

    // Courtesy of Frederik.L, https://stackoverflow.com/questions/16427636/check-if-localstorage-is-available
    private localStorageAvailable(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            if (typeof localStorage !== "undefined") {
                try {
                    localStorage.setItem("bridge", "available");
                    if (localStorage.getItem("bridge") === "available") {
                        localStorage.removeItem("bridge");
                        // localStorage is enabled
                        resolve(true);
                    } else {
                        // localStorage is disabled
                        resolve(false);
                    }
                } catch (e) {
                    // localStorage is disabled
                    resolve(false);
                }
            } else {
                // localStorage is not available
                resolve(false);
            }
        });
    }

    // Get available drivers
    private updateAvailableDrivers(): Promise<void> {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.localStorageAvailable().then(status => {
                    this.drivers.available.localStorage = status;
                })
            ])
                .then(() => {
                    resolve();
                })
                .catch(reject);
        });
    }

    // Check if requested driver is available
    private driverAvailable(driver: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            switch (driver) {
                case "l":
                case "localStorage":
                    this.drivers.available.localStorage
                        ? resolve()
                        : reject(`Driver ${driver} unavailable.`);
                    break;
                case "c":
                case "cookie":
                    this.drivers.available.cookie
                        ? resolve()
                        : reject(`Driver ${driver} unavailable.`);
                    break;
                case "a":
                case "auto":
                    this.drivers.available.auto
                        ? resolve()
                        : reject(`Driver ${driver} unavailable.`);
                    break;
                case "s":
                case "sessionStorage":
                    this.drivers.available.sessionStorage
                        ? resolve()
                        : reject(`Driver ${driver} unavailable.`);
                    break;
                case "i":
                case "indexedDB":
                    this.drivers.available.indexedDB
                        ? resolve()
                        : reject(`Driver ${driver} unavailable.`);
                    break;
                default:
                    reject(`Unknown driver '${driver}'.`);
            }
        });
    }

    // Pick driver when auto is specified
    private autoPickDriver() {}

    // Initialize bridge
    constructor(driver: string = "auto") {
        this.driver = driver;
        this.updateAvailableDrivers().then(() => {
            this.driverAvailable(this.driver)
                .then(() => {
                    // Proceed...
                })
                .catch(error => {
                    throw error;
                });
        });
    }

    // Return promise that resolves when a key is detected
    public when(key: string): Promise<object> {
        return new Promise((resolve, reject) => {
            this.watch(key)
                .then(this.getDataForKey)
                .catch(reject);
        });
    }

    // Check if key exists
    private check(key: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            key = key;
            let tick = setInterval(() => {
                clearInterval(tick);
                resolve(true);
            }, 100);
        });
    }

    // Get data for key
    private getDataForKey(key: string): Promise<object> {
        return new Promise((resolve, reject) => {
            resolve({
                data: key
            });
        });
    }

    // Watch for updates to data
    private watch(key: string): Promise<string> {
        // watch for key
        return new Promise((resolve, reject) => {
            // Code
            this.check(key)
                .then(() => {
                    resolve(key);
                })
                .catch(reject);
        });
    }
}
